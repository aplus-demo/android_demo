package com.aplusjapan.android_demo;

import static com.aplusjapan.android_demo.list.ItemType.AFEvent;
import static com.aplusjapan.android_demo.list.ItemType.APJCenter;
import static com.aplusjapan.android_demo.list.ItemType.GoogleRank;
import static com.aplusjapan.android_demo.list.ItemType.InitSDK;
import static com.aplusjapan.android_demo.list.ItemType.LoginAlert;
import static com.aplusjapan.android_demo.list.ItemType.OpenAIHelp;
import static com.aplusjapan.android_demo.list.ItemType.PayItem1;
import static com.aplusjapan.android_demo.list.ItemType.PayItem2;
import static com.aplusjapan.android_demo.list.ItemType.QueryProduct;
import static com.aplusjapan.android_demo.list.ItemType.SDKVersion;
import static com.aplusjapan.android_demo.list.ItemType.SetLanguage;
import static com.aplusjapan.android_demo.list.ItemType.ShareImageLink;
import static com.aplusjapan.android_demo.list.ItemType.ShareImageLinkList;
import static com.aplusjapan.android_demo.list.ItemType.ShareImagePath;
import static com.aplusjapan.android_demo.list.ItemType.ShareImagePathList;
import static com.aplusjapan.android_demo.list.ItemType.ShareNativeLink;
import static com.aplusjapan.android_demo.list.ItemType.ShareNativePath;
import static com.aplusjapan.android_demo.list.ItemType.ShareWebLink;
import static com.aplusjapan.android_demo.list.ItemType.SyncRole;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;

import com.aplusjapan.android_demo.list.ItemType;
import com.aplusjapan.android_demo.list.ListAdapter;
import com.aplusjapan.aplussdk.base.APJSDK;
import com.aplusjapan.aplussdk.base.common.APJEventPlatform;
import com.aplusjapan.aplussdk.base.common.APJStringUtil;
import com.aplusjapan.aplussdk.base.common.APJUserModel;
import com.aplusjapan.aplussdk.base.language.APJLanguageType;
import com.aplusjapan.aplussdk.base.language.APJLanguageUtil;
import com.aplusjapan.aplussdk.base.network.wrapper.APJSDKCallback;
import com.aplusjapan.aplussdk.base.payment.APJProductModel;
import com.aplusjapan.aplussdk.base.third.APJShareResult;
import com.aplusjapan.sdkdevelop.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity implements APJSDKCallback {

    private ListView listView;
    private ListAdapter adapter;
    private List<ItemType> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideBackButton();

        //添加 List 测试项
        listView = findViewById(R.id.list_view);

        dataList.add(ItemType.SetLanguage);
        dataList.add(InitSDK);
        dataList.add(LoginAlert);
        dataList.add(APJCenter);

        dataList.add(QueryProduct);
        dataList.add(PayItem1);
        dataList.add(PayItem2);

        dataList.add(ShareWebLink);
        dataList.add(ShareImageLink);
        dataList.add(ShareImagePath);
        dataList.add(ShareImageLinkList);
        dataList.add(ShareImagePathList);
        dataList.add(ShareNativeLink);
        dataList.add(ShareNativePath);

        dataList.add(AFEvent);
        dataList.add(SyncRole);
        dataList.add(OpenAIHelp);
        dataList.add(GoogleRank);
        dataList.add(SDKVersion);

        adapter = new ListAdapter(this, R.layout.main_list_item, dataList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemTaped(position, adapter.getItem(position));
            }
        });
    }

    public void itemTaped(int index, ItemType itemType) {
        if (itemType == InitSDK) {
            APJSDK.initSDK(this, APJLanguageType.EN, this);

        } else if (itemType == LoginAlert) {
            APJSDK.startLogin(this);

        } else if (itemType == APJCenter) {
            APJSDK.openAccountCenter(this);

        } else if (itemType == SetLanguage) {
//            Intent intent = new Intent(this, LanguageActivity.class);
//            startActivity(intent);

            //测试广告
//            APJSDK.showGoogleInterstitialAd("ca-app-pub-3940256099942544/5224354917");

        } else if (itemType == PayItem1) {
            APJSDK.buyProduct("com.aplusjapan.sdkdevelop.item1", "aa",
                    "ss", "rr", APJStringUtil.getUUID(),
                    "", "","extraInfo");

        } else if (itemType == PayItem2) {
            APJSDK.buyProduct("com.aplusjapan.sdkdevelop.item2", "aa2",
                    "ss2", "rr2", APJStringUtil.getUUID(),
                    "", "","extraInfo");

        } else if (itemType == QueryProduct) {
            List<String> list = new ArrayList<>();
            list.add("com.aplusjapan.sdkdevelop.item1");
            list.add("com.aplusjapan.sdkdevelop.item2");
            APJSDK.queryProductInfo(list);

        } else if (itemType == ShareWebLink) {
            APJSDK.shareFacebookPathOrUrl("https://www.baidu.com");

        } else if (itemType == ShareImageLink) {
            APJSDK.shareFacebookPathOrUrl("https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg");

        } else if (itemType == ShareImagePath) { //手机本地图片路径
            APJSDK.shareFacebookPathOrUrl("/storage/emulated/0/Pictures/知乎/1683178118527.jpeg");

        } else if (itemType == ShareImageLinkList) {
            List<String> list = new ArrayList<>();
            list.add("https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg");
            list.add("https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg");
            APJSDK.shareFacebookImageList(list);

        } else if (itemType == ShareImagePathList) { //手机本地图片路径
            List<String> list = new ArrayList<>();
            list.add("/storage/emulated/0/Pictures/知乎/1683178118527.jpeg");
            list.add("/storage/emulated/0/Pictures/知乎/1683178118527.jpeg");
            APJSDK.shareFacebookImageList(list);

        } else if (itemType == ShareNativeLink) { //原生分享网络图片
            List<String> list = new ArrayList<>();
            list.add("https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg");
            list.add("https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg");
            APJSDK.shareNativeList(list);

        } else if (itemType == ShareNativePath) {//原生分享本地图片
            List<String> list = new ArrayList<>();
            list.add("/storage/emulated/0/Pictures/知乎/1683178118527.jpeg");
            list.add("/storage/emulated/0/Pictures/知乎/1683178118527.jpeg");
            APJSDK.shareNativeList(list);

        } else if (itemType == AFEvent) {
            Map<String, Object> map = new HashMap<>();
            map.put("af_name_and", "af_value_and");
            APJSDK.trackEvent(APJEventPlatform.AppsflyerEvent, "event_android_name", map);

        } else if (itemType == SyncRole) {
            //字符串，都不能为空
            APJSDK.syncRoleInfo("account_id_and",
                    "server_id_and",
                    "server_name_and",
                    "role_id_and",
                    "role_name_and",
                    "level_1_and");

        } else if (itemType == OpenAIHelp) {
            APJSDK.openAIHelp("Welcome~");

        } else if (itemType == GoogleRank) {
            APJSDK.requestReview();

        }else if (itemType == SDKVersion) {
            Tools.showToast(APJSDK.getSDKVersion());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        APJLanguageType type = APJLanguageUtil.currentLanguageType();
        ItemType item = dataList.get(0);
        item.setTitle("设置SDK语言 " + type.getName());
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }


    //-------------------------APJ SDK 回调-------------------------
    @Override
    public void onApjSDKInitSuccess() {
        Tools.showToast("初始化成功");
    }

    @Override
    public void onApjSDKInitFail(int code, String msg) {
        Tools.showToast("初始化失败：" + msg);
    }

    @Override
    public void onApjSDKLoginSuccess(APJUserModel userModel) {
        Tools.showToast("登陆成功" + userModel.getUserEmail());
    }

    @Override
    public void onApjSDKLoginFail(int code, String msg) {
        Tools.showToast("登陆失败：" + msg);
    }

    @Override
    public void onApjSDKLogoutSuccess() {
        Tools.showToast("退出成功");
    }

    @Override
    public void onApjBindAPJAccountSuccess(String s) {
        Tools.showToast(s);
    }

    @Override
    public void onApjSDKAccountDeleteSuccess() {
        Tools.showToast("删除账号成功");
    }

    @Override
    public void onApjSDKPaymentSuccess(String productId, String apjOrderId, String cpOrderId) {
        Tools.showToast("购买成功：" + productId);
    }

    @Override
    public void onApjSDKPaymentFail(int code, String msg) {
        Tools.showToast("购买失败：" + msg);
    }

    @Override
    public void onApjSDKQueryProductSuccess(@NonNull List<APJProductModel> productDetailsList) {
        Tools.showToast("查询成功！" + productDetailsList.size());
        Tools.print(new Gson().toJson(productDetailsList));
    }

    @Override
    public void onApjSDKQueryProductFail(int code, String msg) {
        Tools.showToast("查询失败！");
    }

    @Override
    public void onApjSDKReceiveNotifyToken(String token) {
        Tools.showToast("收到notify token: " + token);
        Tools.print("收到notify token: " + token);
    }

    @Override
    public void onApjSDKFetchAppsflyerDeepLinkValueSuccess(String deepLinkValue) {
        Tools.showToast("AF deep link value: " + deepLinkValue);
    }

    @Override
    public void onApjSDKShareResult(APJShareResult apjShareResult) {

    }

    @Override
    public void onApjSDKUserType(int i, String s) {

    }

    @Override
    public void onApjSDKSyncRoleInfoResult(boolean b, String s) {

    }

    @Override
    public void onApjEmailRegisterSuccess(String email) {

    }

    @Override
    public void onApjSDKGoogleAdClicked(String adUnitId) {
        Tools.showToast("onApjGoogleAdClicked: " + adUnitId);
        Tools.print("onApjGoogleAdClicked: " + adUnitId);
    }

    @Override
    public void onApjSDKGoogleAdFinishSuccess(String adUnitId) {
        Tools.showToast("onApjGoogleAdDismissed: " + adUnitId);
        Tools.print("onApjGoogleAdDismissed: " + adUnitId);
    }

}