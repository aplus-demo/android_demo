package com.aplusjapan.android_demo.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aplusjapan.sdkdevelop.R;

import java.util.List;

public class ListAdapter extends ArrayAdapter<ItemType> {

    private int resourceId;

    public ListAdapter(@NonNull Context context, int resource, List<ItemType> dataList) {
        super(context, resource, dataList);
        this.resourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemType model = getItem(position);
        View view;
        ViewHolder viewHolder;

        if (convertView == null){
            view = LayoutInflater.from(getContext()).inflate(resourceId, null);
            viewHolder = new ViewHolder();
            viewHolder.title_tv =  view.findViewById(R.id.title);
            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.title_tv.setText(model.getTitle());
        return view;
    }

    class ViewHolder{
        TextView title_tv;
    }

}
