package com.aplusjapan.android_demo.list;

public enum ItemType {
    InitSDK("初始化SDK"),
    LoginAlert("登陆SDK"),
    APJCenter("打开账号中心"),
    SetLanguage("设置SDK语言 "),
    QueryProduct("查询商品信息"),
    PayItem1("购买item1"),
    PayItem2("购买item2"),
    ShareWebLink("facebook分享 网页链接"),
    ShareImageLink("facebook分享 图片链接"),
    ShareImagePath("facebook分享 本地图片路径"),
    ShareImageLinkList("facebook分享 多张 图片链接"),
    ShareImagePathList("facebook分享 多张 本地图片路径"),
    ShareNativeLink("原生分享 图片链接 可多个"),
    ShareNativePath("原生分享 图片路径 可多个"),
    AFEvent("事件埋点"),
    SyncRole("同步用户信息"),
    OpenAIHelp("打开客服"),
    GoogleRank("打开谷歌评分弹框"),
    SDKVersion("APJSDK 版本号"),
    ;
    private String title;

    ItemType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
