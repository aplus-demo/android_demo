package com.aplusjapan.android_demo;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aplusjapan.aplussdk.base.APJSDK;
import com.aplusjapan.aplussdk.base.language.APJLanguageType;
import com.aplusjapan.sdkdevelop.R;


public class LanguageActivity extends BaseActivity {

    private TextView en_tv;
    private TextView de_tv;
    private TextView fr_tv;
    private TextView pt_tv;
    private TextView es_tv;
    private TextView ar_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_second);
        setTitle("设置SDK语言");

        en_tv = findViewById(R.id.en_tv);
        de_tv = findViewById(R.id.de_tv);
        fr_tv = findViewById(R.id.fr_tv);
        pt_tv = findViewById(R.id.pt_tv);
        es_tv = findViewById(R.id.es_tv);
        ar_tv = findViewById(R.id.ar_tv);

        en_tv.setOnClickListener(this);
        de_tv.setOnClickListener(this);
        fr_tv.setOnClickListener(this);
        pt_tv.setOnClickListener(this);
        es_tv.setOnClickListener(this);
        ar_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();
        if (vid == R.id.en_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.EN);

        } else if (vid == R.id.de_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.DE);

        } else if (vid == R.id.fr_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.FR);

        } else if (vid == R.id.pt_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.PT);

        } else if (vid == R.id.es_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.ES);

        } else if (vid == R.id.ar_tv) {
            APJSDK.setSDKLanguage(APJLanguageType.Ar);
        }
        finish();
    }
}