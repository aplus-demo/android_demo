package com.aplusjapan.android_demo;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aplusjapan.sdkdevelop.R;
import com.gyf.immersionbar.BarHide;
import com.gyf.immersionbar.ImmersionBar;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout content_layout;
    private ImageView back_img;
    private TextView title_tv;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        content_layout = findViewById(R.id.content_layout);
        back_img = findViewById(R.id.back_img);
        title_tv = findViewById(R.id.title_tv);
        back_img.setOnClickListener(this);
        LayoutInflater.from(this).inflate(layoutResID, content_layout);
        ImmersionBar.with(this).init();
        ImmersionBar.with(this).transparentStatusBar().hideBar(BarHide.FLAG_HIDE_BAR).init();
    }

    protected void hideBackButton() {
        back_img.setVisibility(View.GONE);
    }

    protected void setTitle(String title) {
        title_tv.setText(title);
    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();
        if (vid == R.id.back_img) {
            finish();
        }
    }
}