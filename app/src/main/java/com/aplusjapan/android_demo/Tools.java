package com.aplusjapan.android_demo;

import android.util.Log;
import android.widget.Toast;

import com.aplusjapan.aplussdk.base.common.APJDeviceUtil;

import java.util.Locale;

public class Tools {
    public static void showToast(String msg){
        APJDeviceUtil.mainThread(()->{
            Toast toast = Toast.makeText(MainApplication.application, "", Toast.LENGTH_SHORT);
            toast.setText(msg);
            toast.show();
        });
    }

    public static void print(String msg){
        Log.d("APJSDKLogger", String.format(Locale.CHINA, "\n-----------APJLogger Start Demo-----------%s\n\n%s\n\n-----------APJLogger End-----------\n", Thread.currentThread().getName(), msg));
    }



}
