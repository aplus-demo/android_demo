# APJSDK

##  Android 接入
`targetSdkVersion 需要 >= 34`, 谷歌后台要求。接口都在 `APJSDK.java` 文件中，回调都在 `APJSDKCallback.java` 文件中。

![01](https://gitlab.com/aplus-demo/android_demo/-/raw/main/Resource/01.jpeg)
![02](https://gitlab.com/aplus-demo/android_demo/-/raw/main/Resource/02.jpeg)
![03](https://gitlab.com/aplus-demo/android_demo/-/raw/main/Resource/03.jpeg)


1. 下载 [APlusSDK.aar](https://gitlab.com/aplus-demo/android_demo/-/blob/main/Resource/APlusSDK.aar) 放入项目的 libs 文件夹中。
2. 下载  `apj_sdk_config.json` [配置文件模板](https://gitlab.com/aplus-demo/android_demo/-/blob/main/Resource/apj_sdk_config.json)，配置都是可选的，没有就不用配置， 配置信息我们给。
3. 将 `google-service.json`文件并添加到项目中，我们会提供该文件，`注意文件位置要在APP模块文件夹中`。
4. 在`build.gradle`中添加库引用。
```
1. 添加配置
plugins {
    id 'com.google.gms.google-services'
    id 'com.google.firebase.crashlytics'
}

1.2 在项目的 setting.gradle 中添加【maven { url 'https://maven.singular.net/' }】如下代码所示
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven { url 'https://maven.singular.net/' }
    }
}

2. 添加引用
dependencies {
    implementation fileTree(dir: "libs")

         //google
    implementation 'com.google.android.play:review:2.0.1'
    implementation 'com.google.android.play:app-update:2.1.0'
    implementation 'com.google.gms:google-services:4.3.15'
    implementation "com.android.billingclient:billing:7.0.0"
    implementation 'com.google.android.gms:play-services-auth:20.5.0'

    //firebase
    implementation platform('com.google.firebase:firebase-bom:32.3.1')
    implementation 'com.google.firebase:firebase-analytics'
    implementation 'com.google.firebase:firebase-crashlytics'
    implementation 'com.google.firebase:firebase-messaging'
    implementation 'com.google.firebase:firebase-auth'
    implementation 'com.google.firebase:firebase-inappmessaging-display'

    //af
    implementation 'com.appsflyer:af-android-sdk:6.14.0'
    implementation 'com.android.installreferrer:installreferrer:2.2'

    //facebook
    implementation 'com.facebook.android:facebook-android-sdk:16.0.1'
    implementation 'com.facebook.android:facebook-share:16.0.1'

    //AIHelp
    implementation 'net.aihelp:android-aihelp-aar:5.3.0'

    //ThinkingData
    implementation 'cn.thinkingdata.android:ThinkingAnalyticsSDK:3.0.2'
    implementation 'cn.thinkingdata.android:TAThirdParty:2.0.0'

    //Segment
    implementation 'com.segment.analytics.kotlin:android:1.16.0'

    //JWT
    implementation 'com.auth0.android:jwtdecode:2.0.2'

    //Singular
    implementation 'com.singular.sdk:singular_sdk:12.5.5'

    //谷歌广告
    implementation 'com.google.android.gms:play-services-ads:23.6.0'
}
 ```
3. 在Project的`build.gradle`中添加配置
```
buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath 'com.google.gms:google-services:4.3.15'
        classpath 'com.google.firebase:firebase-crashlytics-gradle:2.9.2'
    }
}
```
4. 在`AndroidManifest.xml`中添加配置
```
1.权限配置，游戏需要用Facebook分享或者本地分享就需要加，没用分享可以不加
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

2. 在application里添加Facebook配置
 <provider
            android:name="com.facebook.FacebookContentProvider"
            android:authorities="com.facebook.app.FacebookContentProviderXXX" //把XXX替换为游戏的Facebook AppID
            android:exported="true" />

3. 在MainActivity里添加Appsflyer filter:
 <intent-filter android:autoVerify="true">
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data
        android:host="XXX"         //把XXX替换为游戏自己的配置
        android:pathPrefix="/XXX"  //把XXX替换为游戏自己的配置
        android:scheme="https" />
</intent-filter>

4. 设置App theme, 为了使SDK弹框全屏居中 【如果登录弹框没居中，请结合项目参考设置这个信息】
<style name="APJSDKTheme" parent="Theme.AppCompat.NoActionBar">
  <item name="android:windowFullscreen">true</item>
  <item name="android:windowContentOverlay">@null</item>
  <item name="windowActionBar">false</item>
  <item name="windowNoTitle">true</item>
</style>

5. 添加混淆配置
-dontwarn com.appsflyer.**
-keep public class com.google.firebase.messaging.FirebaseMessagingService {
    public *;
}
-dontwarn cn.thinkingdata.thirdparty.**
-keep class cn.thinkingdata.thirdparty.** { *; }
-keep class cn.thinkingdata.module.routes.** { *; }
-keep class com.aplusjapan.aplussdk.** { *; }
-keep class net.aihelp.**{*;}


5. 在初始化SDK的Activity里添加
@Override
protected void onNewIntent(Intent intent) {
  super.onNewIntent(intent);
  APJSDK.checkNewIntent(intent);
}

```

### 2.2 Android 接口使用,引入 import com.aplusjapan.aplussdk.base.APJSDK;

#### APJSDK API 概览
```
public class APJSDK {
    //初始化SDK
    public static void initSDK(Activity activity, APJLanguageType sdkLanguage, APJSDKCallback sdkCallback)

    //单独设置语言
    public static void setSDKLanguage(APJLanguageType languageType)

    //登录弹框
    public static void startLogin(Activity activity)

    //打开个人中心
    public static void openAccountCenter(Activity activity)

    //【重要！】登录成功后同步信息给SDK和第三方库，获取到相关信息后尽快调用，都不能为空或者空字符串！
    //gRoleId 被用作数数SDK的 account_id, 如果服务端有用数数API，请保持一致。
    //数数SDK参考链接：https://docs.thinkingdata.jp/ta-manual/latest/installation/pre_installation/user_identify.html#_1-1-%E8%AE%BF%E5%AE%A2-id-distinct-id
    public static void syncRoleInfo(String gAccountId,
                                    String gServerId,
                                    String gServerName,
                                    String gRoleId, //被用作数数SDK的 account_id
                                    String gRoleName,
                                    String gLevel)

    //购买商品
    public static void buyProduct(String productId,   //必须 购买的商品ID
                                  String cpAccountId, //必须 玩家游戏总账号ID
                                  String cpServerId,  //必须 玩家角色所在服务器ID
                                  String cpRoleId,    //必须 玩家角色ID
                                  String cpOrderId,   //必须 CP订单号
                                  String cpNotifyUrl, //支付完成通知游戏服务器接口URL，游戏提供给我们
                                  String cpRefundUrl, //退款通知接口URL
                                  String cpExtInfo)   //可选，订单穿透信息，会在通知URL中携带给游戏服务器

    //查询商品信息，谷歌商品ID列表
    public static void queryProductInfo(List<String> productIdList)

    返回列表对象如下 
    private String productId; //com.xxx.item2 
    private String name;      //item2
    private String descriptionString; //item2
    private double priceAmountMicros;  //320 货币金额
    private String priceCurrencyCode; //JPY 货币单位
    private String formattedPrice;  //JP¥320 

    
    //事件埋点
    public static void trackEvent(APJEventPlatform platform, @NonNull String eventName, @Nullable Map<String, Object> valueMap);
    public enum APJEventPlatform { //埋点平台
       AppsflyerEvent,
       SingularEvent,
       SegmentEvent,
       ThinkingDataEvent,
       FirebaseEvent;
    }

    //facebook 分享单个 网页链接/图片链接/本地图片路径
    public static void shareFacebookPathOrUrl(String pathOrUrl)

    //facebook 分享 多张图片链接/多张本地图片路径
    public static void shareFacebookImageList(List<String> imagePathOrUrlList)

    //安卓原生分享 多张图片链接/多张本地图片路径
    public static void shareNativeList(List<String> imagePathOrUrlList)

    //打开AIHelp客服页面
    public static void openAIHelp(String welcomeMsg)

    //获取SDK版本号
    public static String getSDKVersion()

    //在游戏支付页面Activity onResume 方法里调用，防止掉单
    public static void onResume()

    //在分享Activity 对应方法里调用
    public static void onActivityResult(int requestCode, int resultCode, Intent data)

    //打开谷歌评分弹框，不是每次都会弹出，提交后后面就不弹出了
    public static void requestReview()

    //获取设备ID，卸载重装不变
    public static String getDeviceId();

    //【重要】获取安装包ID，卸载重装会变。【要把这个给游戏后台用于数数的游客ID!!】
    public static String getInstallId();

    //打开网页，比如用来打开CR客服页面url: https://games.help.crunchyroll.com/hc/en-us
    public static void openWebUrl(String webUrl);

    //检查用户类型
    报错: -1   报错信息
    非会员: 0  is_third_account
    APJ账号: 1  apj_account
    CR账号: 2  cr_account
    CR付费会员: 3  cr_fan
    CR付费会员mega: 4  cr_mega_fan
    CR付费会员ultimate: 5   cr_ultimate_fan
    public static void checkUserVIPType(); 


    //展示谷歌插屏广告
    public static void showGoogleInterstitialAd(String adUnitId);
}

```


#### APJSDKCallback 概览
```
public interface APJSDKCallback {
    void onApjSDKInitSuccess(); //初始化成功

    void onApjSDKInitFail(int code, String msg); //初始化失败

    void onApjEmailRegisterSuccess(String email); //APJ Email 注册成功

    void onApjSDKLoginSuccess(APJUserModel userModel); //登陆成功

    void onApjSDKLoginFail(int code, String msg); //登陆失败

    void onApjSDKLogoutSuccess(); //退出登陆

    void onApjSDKAccountDeleteSuccess(); //提交账号删除成功，30天内不登录后才会销户

    void onApjBindAPJAccountSuccess(String email); //绑定apj账号成功

    void onApjSDKPaymentSuccess(String productId, String apjOrderId, String cpOrderId); //谷歌购买成功

    void onApjSDKPaymentFail(int code, String msg); //谷歌购买失败
    void onApjSDKQueryProductSuccess(@NonNull List<APJProductModel> productDetailsList); //查询商品信息成功
    void onApjSDKQueryProductFail(int code, String msg); //查询商品信息失败
    void onApjSDKReceiveNotifyToken(String token);  //获取注册通知的token
    void onApjSDKFetchAppsflyerDeepLinkValueSuccess(String deepLinkValue);  //获取af deep_link_value
    void onApjSDKShareResult(APJShareResult shareResult);  //分享结果回调

    //报错: -1   报错信息
    //非会员: 0  is_third_account
    //APJ账号: 1  apj_account
    //CR账号: 2  cr_account
    //CR付费会员: 3  cr_fan
    //CR付费会员mega: 4  cr_mega_fan
    //CR付费会员ultimate: 5   cr_ultimate_fan
    void onApjSDKUserType(int userType, String msg);  

    void onApjSDKGoogleAdClicked(String adUnitId);  //谷歌广告：点击广告
    void onApjSDKGoogleAdFinishSuccess(String adUnitId); //谷歌广告：广告关闭
}
```


#### 初始化SDK,初始化成功后才可以使用其他接口
```
APJSDK.initSDK(Activity, APJLanguageType, APJSDKCallback);

回调接口：
void onApjSDKInitSuccess();                   //初始化成功
void onApjSDKInitFail(int code, String msg);  //初始化失败

1.SDK支持语言类型：
public enum APJLanguageType {
    EN("en", "英语"),
    DE("de", "德语"),
    FR("fr", "法语"),
    PT("pt", "葡萄牙语"),
    ES("es", "西班牙语文"),
    Ar("ar", "阿拉伯语"),
    JP("ja", "日语"),
    KR("ko", "韩语"),
    CN("zh-CN", "简中"),
    TW("zh-TW", "繁中");
}

```

#### 登录SDK
```
APJSDK.startLogin(Activity);

回调接口：
void onApjSDKLoginSuccess(APJUserModel userModel); //登陆成功
void onApjSDKLoginFail(int code, String msg);      //登陆失败
```

#### 重置SDK语言
```
APJSDK.setSDKLanguage(APJLanguageType);

SDK支持语言类型:
public enum APJLanguageType {
    EN("en", "英语"),
    DE("de", "德语"),
    FR("fr", "法语"),
    PT("pt", "葡萄牙语"),
    ES("es", "西班牙语文"),
    Ar("ar", "阿拉伯语"),
    JP("ja", "日语"),
    KR("ko", "韩语"),
    CN("zh-CN", "简中"),
    TW("zh-TW", "繁中");
}
```

#### 打开账号中心
```
APJSDK.openAccountCenter(Activity);
```

#### 【重要】同步游戏角色信息给SDK，登录成功后尽快调用，都不能为空或者空字符串！
```
APJSDK.syncRoleInfo(gAccountId, 没账号ID那就和roleId一样
                     gServerId, 服务器ID
                   gServerName, 服务器名称
                       gRoleId, 角色ID
                     gRoleName, 角色昵称
                    gRoleLevel);角色等级
```

#### 获取商品信息列表
```
注意！！
1.查询数量和返回数量可能会不一样的,如果这个商品不存在的话就不会返回这一项。
2.商品列表显示的货币单位和金额正常是和谷歌支付弹框显示的单位和金额一样的,不一样就是有问题。
3.不要有空格或者换行符！注意返回结果和参数不是一一对应的，返回结果数量 <= 查询参数数量！

List<String> list = new ArrayList<>();
list.add("谷歌后台商品ID");
list.add("谷歌后台商品ID");
APJSDK.queryProductInfo(list);

回调接口：
void onApjSDKQueryProductSuccess(@NonNull List<APJProductModel> productDetailsList); //查询商品信息成功
void onApjSDKQueryProductFail(int code, String msg);                                 //查询商品信息失败
```

#### 购买商品
```  
APJSDK.buyProduct(productID,谷歌后台的商品ID
               cpAccountId, 游戏账号ID
                cpServerId, 游戏服务ID
                  cpRoleId, 角色ID
                 cpOrderId, 游戏的订单ID,唯一字符串
               cpNotifyUrl, SDK服务器通知游戏服务器购买结果的url，游戏提供这个url 
                cpRefundUrl, 退款通知接口URL
                 cpExtInfo); 透传信息，会在notify url里给游戏服务器
       
回调接口：
void onApjSDKPaymentSuccess(String productId, String apjOrderId, String cpOrderId); //谷歌购买成功
void onApjSDKPaymentFail(int code, String msg);                                     //谷歌购买失败

【要注意的code:】
-1000: 网络失败
4001: 谷歌支付取消
5201: 月支付额度被限制了，日本地区才有 【今月の課金額はすでに上限を達していますため、制限がかけられています。来月からもう一度試みてください。】

ErrorCode参考：
3997    SERVICE_TIMEOUT
3998    FEATURE_NOT_SUPPORTED
3999    SERVICE_DISCONNECTED
4001    USER_CANCELED  【用户取消购买】
4002    SERVICE_UNAVAILABLE
4003    BILLING_UNAVAILABLE
4004    ITEM_UNAVAILABLE
4005    DEVELOPER_ERROR
4006    ERROR
4007    ITEM_ALREADY_OWNED
4008    ITEM_NOT_OWNED
4100    FAILED_LOAD_PURCHASES
4101    FAILED_TO_INITIALIZE_PURCHASE
4102    INVALID_SIGNATURE
4115    FAILED_TO_ACKNOWLEDGE_PURCHASE
-1000   Network Error
```

#### 事件埋点 key value 是字符串
```
Map<String,Object> map = new HashMap<>();
map.put("string key","string value");
map.put("string key","string value");
APJSDK.trackEvent(platform, 事件名称, map);

 APJEventPlatform { //埋点平台
       AppsflyerEvent,
       SingularEvent,
       SegmentEvent,
       ThinkingDataEvent,
       FirebaseEvent;
    }
```

#### Facebook分享
```
1. 
APJThirdManager.shareFacebookPathOrUrl(pathOrUrl); 分享单个图片链接或者图片路径


2. 
List<String> list = new ArrayList<>(); 图片链接或图片路径不可混合使用
list.add("图片链接 或者 图片路径");
list.add("图片链接 或者 图片路径");
APJSDK.shareFacebookImageList(list); 分享多个图片链接或者图片路径

回调接口：
 void onApjSDKShareResult(APJShareResult shareResult);  //分享结果回调

public enum APJShareResult {
    ShareSuccess(0, "success"),  Facebook
    ShareCancel(1, "cancel"),    Facebook
    ShareFail(2, "fail"),        Facebook 
    NativeShareFinish(3, "native_share_finish"); //本地分享 (成功失败取消都是这个)
}
```

#### App原生分享
```
List<String> list = new ArrayList<>(); 图片链接或图片路径不可混合使用
list.add("图片链接 或者 图片路径");
list.add("图片链接 或者 图片路径");
APJSDK.shareNativeList(list); 分享多个图片链接或者图片路径    
```

#### 打开客服平台

```
APJ 用:  APJSDK.openAIHelp(welcomeMsg); 

CR 用:  APJSDK.openWebUrl("https://games.help.crunchyroll.com/hc/en-us");

```

#### 检查用户类型
```
APJSDK.checkUserVIPType(); 

报错: -1   报错信息
非会员: 0  is_third_account
APJ账号: 1  apj_account
CR账号: 2  cr_account
CR付费会员: 3  cr_fan
CR付费会员mega: 4  cr_mega_fan
CR付费会员ultimate: 5   cr_ultimate_fan

public void onApjSDKUserType(int userType, String msg) {
   -1: 报错
   0: is_other 
   1: is_normal 
   2: is_vip
}
```

#### 获取Appsflyer DeepLinkValue (如果有的话才有回调)
```
APJSDK.fetchAppsflyerDeepLinkValue();

回调接口：
void onApjSDKFetchAppsflyerDeepLinkValueSuccess(String deepLinkValue);  //获取af deep_link_value
```



#### 获取Singular Deep Link
```
 public static String checkSingularDeeplink();
 //可能空, 可能https://xxxx, 可能xxx://xxxxxx

```


#### 在初始化SDK的Activity里调用
```
 @Override
 protected void onNewIntent(Intent intent) {
     super.onNewIntent(intent);
     APJSDK.checkNewIntent(intent);
 }

```

#### 退出登录
```
APJSDK.logout();

回调: onApjSDKLogoutSuccess

```


#### 显示谷歌全屏广告
```
在 AndroidManifest.xml <application> 标签里添加配置：
 <meta-data
        android:name="com.google.android.gms.ads.APPLICATION_ID"
        android:value="谷歌广告后台的APPID"/>

调用方法
APJSDK.showGoogleInterstitialAd(谷歌广告位ID);

回调:
void onApjSDKGoogleAdClicked(String adUnitId);  //谷歌广告：点击广告
void onApjSDKGoogleAdFinishSuccess(String adUnitId); //谷歌广告：广告看完关闭

```



